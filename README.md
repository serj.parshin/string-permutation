# String Permutation

[О комбинаторике](https://en.wikipedia.org/wiki/Combination)

[О перестановках на вики](https://en.wikipedia.org/wiki/Permutation)

[Алгоритм Стейнхауса-Джонсона-Троттера](https://en.wikipedia.org/wiki/Steinhaus%E2%80%93Johnson%E2%80%93Trotter_algorithm)

[Используемая библиотека](https://github.com/dpaukov/combinatoricslib3-example)
