package ru.parshin.test;

import ru.parshin.test.context.Context;

public class Application {
    public static void main(String[] args) {
        Context.getInstance().run();
    }
}
