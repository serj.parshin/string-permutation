package ru.parshin.test.context;

import ru.parshin.test.service.StringService;

import java.util.Scanner;

public class Context implements Runnable {

    private static Context ourInstance = new Context();
    private Scanner input = new Scanner(System.in);
    private StringService stringService = StringService.getInstance();

    public static Context getInstance() {
        return ourInstance;
    }

    private Context() {
    }

    public void run() {
        while (true) {
            System.out.println("Введите строку:");
            String userInput = input.nextLine();
            if (userInput.equals("exit")) {
                return;
            }
            stringService.generateAllFrom(userInput);
            System.out.println("end");
        }
    }

}
