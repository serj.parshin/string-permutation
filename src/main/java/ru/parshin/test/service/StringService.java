package ru.parshin.test.service;

public class StringService {

    private static StringService ourInstance = new StringService();

    public static StringService getInstance() {
        return ourInstance;
    }

    private StringService() {
    }

    public void generateAllFrom(final String userInput) {
        if (userInput == null || userInput.isEmpty()) return;
        final StringCombinator combinator = new StringCombinator(userInput);
        combinator.generateCombinations();
        System.out.println("Найдено комбинаций: " + combinator.getTotal());
    }
}

