package ru.parshin.test.service;

import org.paukov.combinatorics3.Generator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StringCombinator {

    private final Set<String> combinations;
    private final List<Character> chars = new ArrayList<>();

    public StringCombinator(final String userInput) {
        this.combinations = new HashSet<>();
        for (char ch : userInput.toCharArray()) {
            chars.add(ch);
        }
    }

    public void generateCombinations() {
        Generator.permutation(chars)
                .simple().stream()
                .map(String::valueOf)
                .forEach(combinations::add);
        combinations.forEach(System.out::println);
    }

    public int getTotal() {
        return combinations.size();
    }
}
